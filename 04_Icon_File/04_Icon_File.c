//4_Assignment_Icon_File

//1. Including headers and libraries.
#include <Windows.h>
#include "IconHeader.h"

#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")


//2. Declare and define CALLBACK proc.
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

//3. Define entry point function.
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
//4. Create required structure for window creation.
{
	static TCHAR szClassName[] = TEXT("LTG");
	static TCHAR szWindowCaption[] = TEXT("163_Rushabh_Patil: Icon File Assignment");

	HBRUSH hBrush = NULL;
	HCURSOR hCursor = NULL;
	HICON hIcon = NULL;
	HICON hIconSm = NULL;
	HWND hWnd = NULL;

	WNDCLASSEX wndEx;
	MSG msg;

	ZeroMemory(&wndEx, sizeof(WNDCLASSEX));
	ZeroMemory(&msg, sizeof(MSG));

	hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 102, 0));
	if (hBrush == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to obtain brush"), TEXT("GetStockObject"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_HAND);
	if (hCursor == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to load a cursor"), TEXT("LoadCursor"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	//hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(ICON01));
	if (hIcon == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to load an icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}
	
	///hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(ICON01));
	if (hIconSm == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to load a small icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}
	

	//5. 12 member intialization: ceate and define class of custom window
	wndEx.cbSize = sizeof(WNDCLASSEX);
	wndEx.cbClsExtra = 0;
	wndEx.cbWndExtra = 0;
	wndEx.hbrBackground = hBrush;
	wndEx.hCursor = hCursor;
	wndEx.hIcon = hIcon;
	wndEx.hIconSm = hIconSm;
	wndEx.hInstance = hInstance;
	wndEx.lpfnWndProc = WndProc;
	wndEx.lpszClassName = szClassName;
	wndEx.lpszMenuName = NULL;
	wndEx.style = CS_HREDRAW | CS_VREDRAW;

	if (!RegisterClassEx(&wndEx))
	{
		MessageBox((HWND)NULL, TEXT("Cannot register a window class"), TEXT("RegisterClassEx"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	hWnd = CreateWindowEx(WS_EX_APPWINDOW, szClassName, szWindowCaption,
		WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | WS_THICKFRAME,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		(HWND)NULL, (HMENU)NULL, hInstance, NULL);
	if (hWnd == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Could not create an application window"), TEXT("CreateWindowEx"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	while (GetMessage(&msg, (HWND)NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static int cxClient, cyClient, cxChar, cyChar;
	static TCHAR* ppszGroupName[] = { TEXT("Dennis Ritchie Group"),
									  TEXT("Ken Thompson Group"),
									  TEXT("Donald Knuth Group"),
									  TEXT("Alan Turing Group"),
									  TEXT("Linus Torvalds Group"),
									  TEXT("David Cutler Group"),
									  TEXT("Lady Ada Lovelace Group"),
									  TEXT("Jon von Neumann Group"),
									  TEXT("Helen Custer Group"),
									  TEXT("Ted Hoff Group"),
									  TEXT("Grace Hopper Group"),
									  TEXT("Alan Kay Group")
	};

	HDC hdc = NULL;

	TEXTMETRIC tm;
	PAINTSTRUCT ps;

	switch (uMsg)
	{
	case WM_CREATE:
		MessageBox(NULL, TEXT("Click OK to view MSTC group names"), TEXT("Window Created"), MB_OK);
		hdc = GetDC(hWnd);
		GetTextMetrics(hdc, &tm);
		cxChar = tm.tmAveCharWidth;
		cyChar = tm.tmHeight + tm.tmExternalLeading;
		ReleaseDC(hWnd, hdc);
		hdc = NULL;
		break;

	case WM_SIZE:
		cxClient = LOWORD(lParam);
		cyClient = HIWORD(lParam);
		break;

	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		//Quadant Allignment
		//1st quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient / 4, cyClient / 4, ppszGroupName[0], lstrlen(ppszGroupName[0]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient / 4, (int)((cyClient / 4) + 2 * cyChar), ppszGroupName[1], lstrlen(ppszGroupName[1]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient / 4, (int)((cyClient / 4) - 2 * cyChar), ppszGroupName[2], lstrlen(ppszGroupName[2]));

		//2nd quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, cyClient / 4, ppszGroupName[3], lstrlen(ppszGroupName[3]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)((cyClient / 4) + 2 * cyChar), ppszGroupName[4], lstrlen(ppszGroupName[4]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)((cyClient / 4) - 2 * cyChar), ppszGroupName[5], lstrlen(ppszGroupName[5]));

		//3rd quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient / 4, (int)cyClient * 3 / 4, ppszGroupName[6], lstrlen(ppszGroupName[6]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient / 4, (int)((cyClient * 3 / 4) + 2 * cyChar), ppszGroupName[7], lstrlen(ppszGroupName[7]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient / 4, (int)((cyClient * 3 / 4) - 2 * cyChar), ppszGroupName[8], lstrlen(ppszGroupName[8]));

		//4th quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)cyClient * 3 / 4, ppszGroupName[9], lstrlen(ppszGroupName[9]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient * 3 / 4, (int)((cyClient * 3 / 4) + 2 * cyChar), ppszGroupName[10], lstrlen(ppszGroupName[10]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)((cyClient * 3 / 4) - 2 * cyChar), ppszGroupName[11], lstrlen(ppszGroupName[11]));

		EndPaint(hWnd, &ps);
		hdc = NULL;
		break;

	case WM_DESTROY:
		MessageBox(NULL, TEXT("All group names assignment closed"), TEXT("Window Destroyed"), MB_OK);
		PostQuitMessage(EXIT_SUCCESS);
		break;
	}

	return (DefWindowProc(hWnd, uMsg, wParam, lParam));
}

/*Problems faced:
1>CVTRES : fatal error CVT1100: duplicate resource.  type:ICON, name:1, language:0x0409
solution: We should not define ICON resource in two resource files and use both the resource files in our solution.
So I removed(commented) line no 30 and 31 in 04_Icon_File.ico file.
Useful references below:
https://social.msdn.microsoft.com/Forums/vstudio/en-US/05e519db-00ed-4fdb-a620-8a9af9e6a792/cvtres-fatal-error-cvt1100-duplicate-resource-typeicon-name1-language0x0409?forum=vclanguage
https://stackoverflow.com/questions/31323596/fatal-error-cvt1100-duplicate-resource-typeicon-name1-c-visual-studio-c
*/