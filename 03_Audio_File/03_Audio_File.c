// 3_Assignment_Audio_File.c : Defines the entry point for the application.

#include <Windows.h>

#pragma comment(lib, "gdi32.lib") 
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "Winmm.lib")																						//Microsoft's library for enabling multimedia support in Windows-based applications to use sound and video.
#include "Header_Music_Resource.h"

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	static TCHAR szClassName[] = TEXT("LTG");
	static TCHAR szWindowCaption[] = TEXT("163_Rushabh_Patil: Music File Assignment");

	HBRUSH hBrush = NULL;
	HCURSOR hCursor = NULL;
	HICON hIcon = NULL;
	HICON hIconSm = NULL;
	HWND hWnd = NULL;

	WNDCLASSEX wndEx;
	MSG msg;

	ZeroMemory(&wndEx, sizeof(WNDCLASSEX));
	ZeroMemory(&msg, sizeof(MSG));

	hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 102, 0));
	if (hBrush == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to obtain brush"), TEXT("GetStockObject"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_HAND);
	if (hCursor == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to load a cursor"), TEXT("LoadCursor"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if (hIcon == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to load an icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if (hIconSm == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to load a small icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	wndEx.cbSize = sizeof(WNDCLASSEX);
	wndEx.cbClsExtra = 0;
	wndEx.cbWndExtra = 0;
	wndEx.hbrBackground = hBrush;
	wndEx.hCursor = hCursor;
	wndEx.hIcon = hIcon;
	wndEx.hIconSm = hIconSm;
	wndEx.hInstance = hInstance;
	wndEx.lpfnWndProc = WndProc;
	wndEx.lpszClassName = szClassName;
	wndEx.lpszMenuName = NULL;
	wndEx.style = CS_HREDRAW | CS_VREDRAW;

	if (!RegisterClassEx(&wndEx))
	{
		MessageBox((HWND)NULL, TEXT("Cannot register a window class"), TEXT("RegisterClassEx"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hWnd = CreateWindowEx(WS_EX_APPWINDOW, szClassName, szWindowCaption,
		WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SYSMENU | WS_THICKFRAME,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		(HWND)NULL, (HMENU)NULL, hInstance, NULL);
	if (hWnd == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Could not create an application window"), TEXT("CreateWindowEx"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	ShowWindow(hWnd, nShowCmd);
	UpdateWindow(hWnd);

	while (GetMessage(&msg, (HWND)NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static int cxClient, cyClient, cxChar, cyChar;
	static TCHAR* ppszGroupName[] = { TEXT("Dennis Ritchie Group"),
									  TEXT("Ken Thompson Group"),
									  TEXT("Donald Knuth Group"),
									  TEXT("Alan Turing Group"),
									  TEXT("Linus Torvalds Group"),
									  TEXT("David Cutler Group"),
									  TEXT("Lady Ada Lovelace Group"),
									  TEXT("Jon von Neumann Group"),
									  TEXT("Helen Custer Group"),
									  TEXT("Ted Hoff Group"),
									  TEXT("Grace Hopper Group"),
									  TEXT("Alan Kay Group")
	};

	HDC hdc = NULL;

	TEXTMETRIC tm;
	PAINTSTRUCT ps;

	switch (uMsg)
	{
	case WM_CREATE:
		MessageBox(NULL, TEXT("Click OK to view Audio File Assignment"), TEXT("Window Created"), MB_OK);
		hdc = GetDC(hWnd);
		GetTextMetrics(hdc, &tm);
		cxChar = tm.tmAveCharWidth;
		cyChar = tm.tmHeight + tm.tmExternalLeading;
		ReleaseDC(hWnd, hdc);
		hdc = NULL;
		break;

	case WM_SIZE:
		cxClient = LOWORD(lParam);
		cyClient = HIWORD(lParam);
		break;

	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		/*	//Row no.1
			SetTextAlign(hdc, TA_LEFT | TA_TOP);   // (TA_LEFT | TA_TOP is for alligning the text as Top-left)
			TextOut(hdc, 0, 0, ppszGroupName[0], lstrlen(ppszGroupName[0]));

			SetTextAlign(hdc, TA_CENTER | TA_TOP);
			TextOut(hdc, cxClient / 2, 0, ppszGroupName[1], lstrlen(ppszGroupName[1]));

			SetTextAlign(hdc, TA_RIGHT | TA_TOP);
			TextOut(hdc, cxClient, 0, ppszGroupName[2], lstrlen(ppszGroupName[2]));

			//Row no.2
			SetTextAlign(hdc, TA_LEFT | TA_TOP);
			TextOut(hdc, 0, cyClient / 2, ppszGroupName[3], lstrlen(ppszGroupName[3]));

			SetTextAlign(hdc, TA_CENTER | TA_TOP);
			TextOut(hdc, cxClient / 2, cyClient / 2, ppszGroupName[4], lstrlen(ppszGroupName[4]));

			SetTextAlign(hdc, TA_RIGHT | TA_TOP);
			TextOut(hdc, cxClient, cyClient / 2, ppszGroupName[5], lstrlen(ppszGroupName[5]));

			//Row no.3
			SetTextAlign(hdc, TA_LEFT | TA_BOTTOM);
			TextOut(hdc, 0, cyClient, ppszGroupName[6], lstrlen(ppszGroupName[6]));

			SetTextAlign(hdc, TA_CENTER | TA_BOTTOM);
			TextOut(hdc, cxClient / 2, cyClient, ppszGroupName[7], lstrlen(ppszGroupName[7]));

			SetTextAlign(hdc, TA_RIGHT | TA_BOTTOM);
			TextOut(hdc, cxClient, cyClient, ppszGroupName[8], lstrlen(ppszGroupName[8]));
		*/

		//Quadant Allignment
		//1st quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient / 4, cyClient / 4, ppszGroupName[0], lstrlen(ppszGroupName[0]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient / 4, (int)((cyClient / 4) + 2 * cyChar), ppszGroupName[1], lstrlen(ppszGroupName[1]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient / 4, (int)((cyClient / 4) - 2 * cyChar), ppszGroupName[2], lstrlen(ppszGroupName[2]));

		//2nd quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, cyClient / 4, ppszGroupName[3], lstrlen(ppszGroupName[3]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)((cyClient / 4) + 2 * cyChar), ppszGroupName[4], lstrlen(ppszGroupName[4]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)((cyClient / 4) - 2 * cyChar), ppszGroupName[5], lstrlen(ppszGroupName[5]));

		//3rd quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient / 4, (int)cyClient * 3 / 4, ppszGroupName[6], lstrlen(ppszGroupName[6]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient / 4, (int)((cyClient * 3 / 4) + 2 * cyChar), ppszGroupName[7], lstrlen(ppszGroupName[7]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient / 4, (int)((cyClient * 3 / 4) - 2 * cyChar), ppszGroupName[8], lstrlen(ppszGroupName[8]));

		//4th quadrant
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)cyClient * 3 / 4, ppszGroupName[9], lstrlen(ppszGroupName[9]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, cxClient * 3 / 4, (int)((cyClient * 3 / 4) + 2 * cyChar), ppszGroupName[10], lstrlen(ppszGroupName[10]));
		SetTextAlign(hdc, TA_CENTER | TA_CENTER);
		TextOut(hdc, (int)cxClient * 3 / 4, (int)((cyClient * 3 / 4) - 2 * cyChar), ppszGroupName[11], lstrlen(ppszGroupName[11]));

		EndPaint(hWnd, &ps);
		hdc = NULL;
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'm':
		case 'M':
			PlaySound(TEXT("music.wav"), NULL, SND_ASYNC| SND_FILENAME);															//Default music will play after typing 'm' OR 'M' on the keyboard.
			break;

		case 'n':
		case 'N':
			PlaySound(MAKEINTRESOURCE(IDC_CUSTOM_MUSIC), NULL, SND_RESOURCE);												        //Custom music will play after typing 'n' OR 'N' on the keyboard.
			break;
		}
		break;

	case WM_DESTROY:
		MessageBox(NULL, TEXT("Audio File assignment closed"), TEXT("Window Destroyed"), MB_OK);
		PostQuitMessage(EXIT_SUCCESS);
		break;
	}

	return (DefWindowProc(hWnd, uMsg, wParam, lParam));
}

/*References for Audio File Assignment:
https://docs.microsoft.com/en-us/previous-versions/dd743680(v=vs.85)
https://docs.microsoft.com/en-us/windows/win32/multimedia/playing-wave-resources?redirectedfrom=MSDN
Wave file downloaded from https://www2.cs.uic.edu/~i101/SoundFiles/
*/
